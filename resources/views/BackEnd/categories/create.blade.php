@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Category</h1>
@stop

@section('content')
    <div class="container">
        <div class="row">
            <div class="offset-3 col-md-5 my-lg-5">   
                <div class="card">
                    <div class="card-header text-center">
                        Add Category
                    </div>
                    <div class="card-body">
                        <form action="{{Route('store_category')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            @method('put')
                            <div class="form-group">
                                <label for="">Category</label>
                                <input type="text" class="form-control" name="category_name" id="category_name" placeholder="--Name" value="{{old('category_name')}}">
                            </div>
                            <div class="form-group">
                                <label for="">Title</label>
                                <input type="text" class="form-control" name="title" id="title" placeholder="--Title" value="{{old('title')}}">
                            </div>
                            <div class="form-group">
                                <label for="">Image_name</label>
                                <input type="file" class="form-control" name="file_upload" id="image_name" placeholder="--Image_name" value="{{old('image_name')}}">
                            </div>
                            <div class="form-group">
                                <label for="">Featured</label>
                                <input type="text" class="form-control" name="featured" id="featured" placeholder="--Featured" value="{{old('featured')}}">
                            </div>
                            <div class="form-group">
                                <label for="">Active</label>
                                <div class="radio">
                                    <input type="radio" name="active" value="Active">Active
                                    <input type="radio" name="active" value="Inactive">Inactive
                                </div>
                            </div>
                            <button type="submit" value="Save" name="btn" class="btn btn-outline-primary btn-block">Category Add</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop