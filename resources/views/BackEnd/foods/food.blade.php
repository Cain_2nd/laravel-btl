@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Food</h1>
@stop

@section('content')
<a href="{{Route('create_food')}}" class="btn btn-success btn-sm" title="Add Food">
    Add Food
</a>
<br/>
<br/>
<div class="table-responsive">
    <table class="table">
        <thead>
            <tr>
                <th>STT</th>
                <th>Title</th>
                <th>Image_name</th>
                <th>Price</th>
                <th>Featured</th>
                <th>Desciption</th>
                <th>Category</th>
                <th>Active</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($foods as $item)
            <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{$item->title}}</td>
                <td><img src="uploads/images/foods/{{$item->image_name}}" class="w-8/12 mb-8 shadow-xl" alt=""></td>
                <td>{{$item->price}}</td>
                <td>{{$item->featured}}</td>
                <td>{{$item->desciption}}</td>
                <td>{{$item->category->category_name}}</td>
                <td>{{$item->active}}</td>
                <td>
                    <a href="{{Route('edit_food',['id'=>$item->id])}}" title="Edit Food"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true">Edit</i></button>
                    <form method="POST" action="{{Route('delete_food',['id'=>$item->id])}}" accept-charset="UTF-8" style="display:inline">
                        @method('DELETE')
                        @csrf
                        <button title="Delete Food" onclick="return confirm('Confirm delete?')" type="submit" class="btn-danger btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true">Delete</i></button>
                    </form>    
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop