<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CheckOutController;
use App\Http\Controllers\FoodController;
use App\Http\Controllers\FoodsController;
use App\Http\Controllers\OrderController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/category',[CategoryController::class,'index'])->name('show_category');
Route::get('/category/add',[CategoryController::class,'create'])->name('create_category');
Route::put('/category/store',[CategoryController::class, 'store'])->name('store_category');
Route::get('/category/edit/{id}', [CategoryController::class, 'edit'])->name('edit_category');
Route::put('/category/update/{id}',[CategoryController::class, 'update'])->name('update_category');
Route::delete('/category/delete/{id}',[CategoryController::class, 'destroy'])->name('delete_category');

Route::get('/food',[FoodController::class,'index'])->name('show_food');
Route::get('/food/add',[FoodController::class,'create'])->name('create_food');
Route::put('/food/store',[FoodController::class, 'store'])->name('store_food');
Route::get('/food/edit/{id}', [FoodController::class, 'edit'])->name('edit_food');
Route::put('/food/update/{id}',[FoodController::class, 'update'])->name('update_food');
Route::delete('/food/delete/{id}',[FoodController::class, 'destroy'])->name('delete_food');

Route::post('/payment', [CheckOutController::class, 'payment'])->name('payment');
Route::get('/returnPayment', [CheckOutController::class, 'returnPayment'])->name('return.payment');

Route::get('/customer/foods',[FoodsController::class,'index'])->name('customer_food');

Route::post('/customer/foods/order/{food}',[OrderController::class,'store'])->name('store_order');
Route::get('/customer/foods/order/show',[OrderController::class,'index'])->name('show_order');
Route::get('/customer/foods/order/{order}', [OrderController::class, 'destroy'])->name('delete_order');


Route::get('/customer', function () {
    return view('Frontend/index');
});
Route::get('/customer/help', function () {
    return view('Frontend/helps/help')->name('help');
});
Route::get('/customer/contact', function () {
    return view('Frontend/contacts/contact')->name('contact');
});
Route::get('/customer/career', function () {
    return view('Frontend/careers/career')->name('career');
});
Route::get('/customer/offer', function () {
    return view('Frontend/offers/offers')->name('offer');
});
Route::get('/customer/privacy', function () {
    return view('Frontend/privacys/privacy')->name('privacy');
});
Route::get('/customer/term', function () {
    return view('Frontend/terms/term')->name('term');
});
Route::get('/customer/about', function () {
    return view('Frontend/abouts/about')->name('about');
});