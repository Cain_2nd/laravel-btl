<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <section class="ftco-section goto-here">
        <div class="container">
            <div class="header clearfix">
                <h3 class="text-muted">VNPAY RESPONSE</h3>
            </div>
            <div class="table-responsive">
                    <div class="form-group">
                        <label>Mã đơn hàng:</label>
                        <label>{{Request::get('vnp_TxnRef')}}</label>
                    </div>
                    <div class="form-group">
                        <label>Số tiền:</label>
                        <label>{{Request::get('vnp_Amount')}}</label>
                    </div>
                    <div class="form-group">
                        <label>Nội dung thanh toán:</label>
                        <label>{{Request::get('vnp_OrderInfo')}}</label>
                    </div>
                    <div class="form-group">
                        <label>Mã phản hồi (vnp_ResponseCode):</label>
                        <label>{{Request::get('vnp_ResponseCode')}}</label>
                    </div>
                    <div class="form-group">
                        <label>Mã giao dịch tại VNPAY:</label>
                        <label>{{Request::get('vnp_TransactionNo')}}</label>
                    </div>
                    <div class="form-group">
                        <label>Mã Ngân hàng:</label>
                        <label>{{Request::get('vnp_BankCode')}}</label>
                    </div>
                    <div class="form-group">
                        <label>Thời gian thanh toán:</label>
                        <label>{{Request::get('vnp_PayDate')}}</label>
                    </div>
                    <div class="form-group">
                        <a href="" class="btn btn-primary">Trở lại</a>
                    </div>
            </div>
        </div>
    </section>
</body>
</html>