<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FoodModel extends Model
{
    use HasFactory;
    protected $table ='foods';
    protected $primaryKey = 'id';
    protected $fillable = [
        'title','image_name','price','featured','desciption','active'
    ];

    public function category()
    {
        return $this->belongsTo(CategoryModel::class, 'category_id','id');
    }
}

